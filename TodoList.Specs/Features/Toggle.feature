﻿Feature: Toggle

A short summary of the feature

@tag1
Scenario: User wants to toggle checkbox of todo item
	Given item exists in todo list
	When user clicks checkbox
	Then checkbox value for todo item should toggle
