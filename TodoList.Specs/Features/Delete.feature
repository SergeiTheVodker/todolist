﻿Feature: Delete

A short summary of the feature

@tag1
Scenario: User wants to delete item from todo list
	Given User has selected todo list item
	When User clicks delete
	Then item is removed from todo list
