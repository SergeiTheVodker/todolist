﻿using NUnit.Framework;
using System.IO;
using TechTalk.SpecFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Models;
using TodoList.Specs.Support.E2E;
using TodoList.ViewModel;

namespace TodoList.Specs.StepDefinitions
{
    [Binding]
    class ToggleStepDefinitions
    {
        [Given(@"item exists in todo list")]
        public void GivenItemExistsInTodoList()
        {
            throw new PendingStepException();
        }

        [When(@"user clicks checkbox")]
        public void WhenUserClicksCheckbox()
        {
            throw new PendingStepException();
        }

        [Then(@"checkbox value for todo item should toggle")]
        public void ThenCheckboxValueForTodoItemShouldToggle()
        {
            throw new PendingStepException();
        }

    }
}
