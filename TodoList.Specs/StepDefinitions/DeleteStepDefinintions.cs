﻿using NUnit.Framework;
using System.IO;
using TechTalk.SpecFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Models;
using TodoList.Specs.Support.E2E;
using TodoList.ViewModel;

namespace TodoList.Specs.StepDefinitions
{
    [Binding]
    class DeleteStepDefinintions
    {
        [Given(@"User has selected todo list item")]
        public void GivenUserHasSelectedTodoListItem()
        {
            throw new PendingStepException();
        }

        [When(@"User clicks delete")]
        public void WhenUserClicksDelete()
        {
            throw new PendingStepException();
        }

        [Then(@"item is removed from todo list")]
        public void ThenItemIsRemovedFromTodoList()
        {
            throw new PendingStepException();
        }

    }
}
