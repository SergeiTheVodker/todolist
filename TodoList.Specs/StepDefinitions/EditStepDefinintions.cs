﻿using NUnit.Framework;
using System.IO;
using TechTalk.SpecFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Models;
using TodoList.Specs.Support.E2E;
using TodoList.ViewModel;

namespace TodoList.Specs.StepDefinitions
{
    [Binding]
    class EditStepDefinintions
    {
        [Given(@"Text box is filled")]
        public void GivenTextBoxIsFilled()
        {
            throw new PendingStepException();
        }

        [When(@"User clicks edit")]
        public void WhenUserClicksEdit()
        {
            throw new PendingStepException();
        }

        [Then(@"Item is modified")]
        public void ThenItemIsModified()
        {
            throw new PendingStepException();
        }

    }
}
