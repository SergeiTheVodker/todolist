﻿using NUnit.Framework;
using System.IO;
using TechTalk.SpecFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Models;
using TodoList.Specs.Support.E2E;
using TodoList.ViewModel;

namespace TodoList.Specs.StepDefinitions
{
    [Binding]
    class AddStepDefinitions
    {
        [Given(@"textbox is filled")]
        public void GivenTextboxIsFilled()
        {
            throw new PendingStepException();
        }

        [When(@"user clicks add")]
        public void WhenUserClicksAdd()
        {
            throw new PendingStepException();
        }

        [Then(@"item is added to todo list")]
        public void ThenItemIsAddedToTodoList()
        {
            throw new PendingStepException();
        }

    }
}
