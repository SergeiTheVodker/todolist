﻿using FlaUI.Core;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Capturing;
using FlaUI.Core.Definitions;
using FlaUI.Core.Tools;
using FlaUI.UIA3;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;


namespace TodoList.Specs.Support.E2E
{
    public class BaseE2E
    {
        protected Application App { get; set; }
        public string RealAppName { get; set; }
        protected UIA3Automation Automation { get; set; }
        protected Window Window { get; set; }
        protected string AppName { get; set; }
        public string ScreenshotsLocation { get; set; }

        protected AutomationElement Desktop
        {
            get
            {
                return Window != null ? Window.Automation.GetDesktop() : Automation.GetDesktop();
            }
        }

        #region Open/Close
        /// <summary>
        /// Opens the app, the realAppName is used because sometimes the app is just a bootstrapper to the real app
        /// </summary>
        /// <param name="appPath"></param>
        /// <param name="realAppName"></param>
        protected void OpenApp(string appPath = "", string realAppName = "")
        {
            RealAppName = realAppName;

            App = Application.Launch(appPath);
            Thread.Sleep(1000);

            // If realAppName is provided then we will wait for the real app to load
            if (!string.IsNullOrEmpty(RealAppName))
            {
                var processId = -1;
                Retry.WhileTrue(() =>
                {
                    processId = GetRunningApp(RealAppName);
                    return processId == -1;
                }, TimeSpan.FromMinutes(10));

                // Attach the real app
                App = Application.Attach(processId);
            }

            Automation = new UIA3Automation();
            // Increase the UIA timeout
            Automation.TransactionTimeout = TimeSpan.FromMinutes(20);
            Window = App.GetMainWindow(Automation);

            Assert.IsNotNull(Window);

            // Let app rest
            Thread.Sleep(500);
        }

        /// <summary>
        /// Close the app being tested
        /// </summary>
        protected void CloseApp()
        {
            Window.Close();
        }

        /// <summary>
        /// Waits for the app to be closed
        /// </summary>
        protected void WaitForAppToClose()
        {
            var desktop = Desktop;

            Retry.WhileNotNull(() =>
            {
                var dialog = desktop.FindFirstDescendant(cf => cf.ByName(AppName))?.AsWindow();
                return dialog;
            }, TimeSpan.FromSeconds(15));

        }

        /// <summary>
        /// Force close the app
        /// </summary>
        /// <param name="appName"></param>
        protected void ForceCloseApp(string appName)
        {
            appName = appName.ToLower();

            var procs = Process.GetProcesses();
            foreach (var proc in procs)
            {
                string procName = proc.ProcessName.ToLower();
                if (proc.ProcessName.ToLower().Contains(appName))
                {
                    proc.Kill();
                    proc.WaitForExit();
                }
            }
        }

        /// <summary>
        /// Checks if app is running
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns></returns>
        protected int CheckIfAppOpen(TimeSpan timeout)
        {
            Window mainWindow = Desktop.FindFirstChild(cf => cf.ByName(AppName))?.AsWindow();
            Retry.WhileNull(() =>
            {
                mainWindow = Desktop.FindFirstChild(cf => cf.ByName(AppName))?.AsWindow();
                return mainWindow;
            }, timeout);
            Assert.IsTrue(mainWindow != null);
            return mainWindow.Properties.ProcessId;
        }

        /// <summary>
        /// Gets the ID of the given app name
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        protected int GetRunningApp(string appName)
        {
            try
            {
                Process[] processes = Process.GetProcessesByName(appName);

                if (processes.Length > 0)
                {
                    return processes[0].Id;
                }

                return -1;
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// Gets the id of the given appname and window title
        /// </summary>
        /// <param name="appName"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        protected int GetRunningApp(string appName, string title)
        {
            try
            {
                Process[] processes = Process.GetProcessesByName(appName);
                // Remove the processes who is still in background process
                processes = processes.Where(proc => proc.MainWindowHandle != IntPtr.Zero).ToArray();
                // Get only those with these title
                processes = processes.Where(proc => proc.MainWindowTitle.Equals(title, StringComparison.InvariantCultureIgnoreCase)).ToArray();

                if (processes.Length > 0)
                {
                    return processes[0].Id;
                }
                return -1;
            }
            catch
            {
                return -1;
            }
        }
        #endregion

        /// <summary>
        /// Wait for element
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="getter"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        protected T WaitForElement<T>(Func<T> getter, TimeSpan timeout)
        {
            var retry = Retry.WhileNull<T>(
                () => getter(),
                timeout);

            if (!retry.Success)
            {
                return default(T);
            }

            return retry.Result;
        }

        /// <summary>
        /// Wait for element with default time 30 seconds
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="getter"></param>
        /// <returns></returns>
        protected T WaitForElement<T>(Func<T> getter)
        {
            return WaitForElement(getter, TimeSpan.FromSeconds(30));
        }

        /// <summary>
        /// Wait for element to be hidden
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="getter"></param>
        /// <param name="timeout"></param>
        protected void WaitForElementHidden<T>(Func<T> getter, TimeSpan timeout)
        {
            var retry = Retry.WhileNotNull<T>(
                () => getter(),
                timeout);
        }

        /// <summary>
        /// Wait for element to be hidden with default time 30 seconds
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="getter"></param>
        protected void WaitForElementHidden<T>(Func<T> getter)
        {
            WaitForElementHidden(getter, TimeSpan.FromSeconds(30));
        }

        /// <summary>
        /// Wait for element with given truthy condition
        /// </summary>
        /// <param name="getter"></param>
        /// <param name="timeout"></param>
        protected void WaitForElementTruthy(Func<bool> getter, TimeSpan timeout)
        {
            var retry = Retry.WhileFalse(
                () => getter(),
                timeout);
        }

        /// <summary>
        /// Wait for element with given truthy condition with default time 30 seconds
        /// </summary>
        /// <param name="getter"></param>
        protected void WaitForElementTruthy(Func<bool> getter)
        {
            WaitForElementTruthy(getter, TimeSpan.FromSeconds(30));
        }

        /// <summary>
        /// Take a screenshot
        /// </summary>
        /// <param name="sleep"></param>
        /// <returns></returns>
        protected Bitmap CaptureWindow(int sleep = 1000)
        {
            Thread.Sleep(sleep);
            return Capture.Element(Window).Bitmap;
        }

        /// <summary>
        /// Take a screenshot then scroll
        /// </summary>
        /// <param name="scroll"></param>
        /// <param name="sleep"></param>
        /// <returns></returns>
        protected IEnumerable<Bitmap> CaptureWindowScroll(AutomationElement scroll, int sleep = 1000)
        {
            var captures = new List<Bitmap>();
            Thread.Sleep(sleep);

            var scrollPercentage = scroll.Patterns.Scroll.Pattern.VerticalScrollPercent;
            captures.Add(Capture.Element(Window).Bitmap);

            while (scrollPercentage <= 98 && scrollPercentage != -1)
            {
                // Scroll it
                scroll.Patterns.Scroll.Pattern.Scroll(ScrollAmount.NoAmount, ScrollAmount.LargeIncrement);
                Thread.Sleep(200);

                // Take a screenshot
                captures.Add(Capture.Element(Window).Bitmap);

                // Calculate again
                scrollPercentage = scroll.Patterns.Scroll.Pattern.VerticalScrollPercent;
            }

            return captures;
        }

        #region App specific functions

        /// <summary>
        /// Retrieves the list of Todo items
        /// TODO: must also return the check state
        /// </summary>
        /// <returns></returns>
        public IList<string> GetTodoItems()
        {
            var items = new List<string>();
            var listViewTodoItems = Window.FindFirstDescendant("listViewTodoItems")?.AsDataGridView();

            foreach (var row in listViewTodoItems.Rows)
            {
                items.Add(row.Cells[1].Name);
            }

            return items;
        }

        #endregion

    }
}
