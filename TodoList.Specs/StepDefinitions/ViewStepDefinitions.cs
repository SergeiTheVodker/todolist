using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TechTalk.SpecFlow;
using TodoList.Models;
using TodoList.Specs.Support.E2E;
using TodoList.ViewModel;

namespace TodoList.Specs.StepDefinitions
{
    [Binding]
    public class ViewStepDefinitions : BaseE2E
    {
        private IList<TodoItem> Items = new List<TodoItem>();

        [Given(@"the user have todos in the list")]
        public void GivenTheUserHaveTodosInTheList()
        {
            Items.Add(new TodoItem() { Description = "Eat burger" });
            Items.Add(new TodoItem() { Description = "Eat fries" });
            Items.Add(new TodoItem() { Description = "Drink water" });
            Items.Add(new TodoItem() { Description = "Drink coke" });
        }

        [When(@"user opens the app")]
        public void WhenUserOpensTheApp()
        {
            var appPath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\..\", "TodoList", "bin", "Debug", "TodoList.exe"));
            OpenApp(appPath);
        }

        [Then(@"user should see the list of todos")]
        public void ThenUserShouldSeeTheListOfTodos()
        {
            var items = GetTodoItems();
            var isAllInThere = Items.All(item => items.FirstOrDefault(i => i.Equals(item.Description)) != null);
            Assert.True(isAllInThere);

            CloseApp();
        }
    }
}
