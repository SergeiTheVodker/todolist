using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using TodoList.Models;
using System.Windows.Input;


namespace TodoList.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        
        public const string ItemNameTextBox = "TextBoxItemName";
        private string _itemName;
        public string TextBoxItemName
        {
            get { return _itemName; }
            set
            {
                if (!string.Equals(this._itemName, value))
                {
                    Set(ItemNameTextBox, ref this._itemName, value);
                    RaisePropertyChanged(ItemNameTextBox);
                }
            }
        }

        public const string selectedItem = "SelectedItem";
        private TodoItem _selectedItem;
        public TodoItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                Set(selectedItem, ref this._selectedItem, value);
               // Set(checkboxValue, ref this._isDone, this._selectedItem.IsDone);
                RaisePropertyChanged(selectedItem);
            }
        }

        private RelayCommand _addItemCommand;
        public RelayCommand AddItemCommand
        {
            get
            {
                return _addItemCommand
                    ?? (_addItemCommand = new RelayCommand(() => {
                        AddItem(_itemName);
                    }));
            }
        }

        private RelayCommand _markItem;
        public RelayCommand MarkItemCommand
        {
            get
            {
                return _markItem ?? (_markItem = new RelayCommand(() => { MarkItem(); }));
            }
        }

        private RelayCommand _editItem;
        
        public RelayCommand EditItemCommand
        {
            get
            {
                return _editItem ?? (_editItem = new RelayCommand(()=> { EditItem(_selectedItem,_itemName); }));
            }
        }

        private RelayCommand _deleteItemCommand;
        public RelayCommand DeleteItemCommand
        {
            get
            {
                return _deleteItemCommand
                    ?? (_deleteItemCommand = new RelayCommand(() => {
                        deleteItem(Items, _selectedItem);//fix this
                    }));
            }
        }
        

        public void MarkItem()
        {
            if(_selectedItem != null)
            {
                _selectedItem.IsDone = !_selectedItem.IsDone;
            }
        }
        public void deleteItem(ObservableCollection<TodoItem> item, TodoItem removeItem)
        {
            item.Remove(removeItem);
        }
        public void AddItem(string ItemName)
        {
            if(ItemName != null)
            {
                Items.Add(new TodoItem() { Description = ItemName, IsDone = false });
                _itemName = null;
                RaisePropertyChanged(ItemNameTextBox);
            }
        }

        public void EditItem(TodoItem itemToEdit ,string newDescription)
        {
            if((newDescription != null) && (newDescription != itemToEdit.Description) && (itemToEdit != null))
            {
                TodoItem newItem = new TodoItem() { Description = itemToEdit.Description, IsDone = itemToEdit.IsDone };
                int index = Items.IndexOf(itemToEdit);
                newItem.Description = newDescription;
                Items.Insert(Items.IndexOf(itemToEdit), newItem);
                Items.Remove(itemToEdit);
                _itemName = null;
                RaisePropertyChanged(ItemNameTextBox);
            }
        }
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            // Bind the collectionchanged, this will let us know if an item in the Items list is added or removed
            Items.CollectionChanged += Items_CollectionChanged;

            if (IsInDesignMode)
            {
                // Code runs in Blend --> create design time data.
                Items.Add(new TodoItem() { Description = "Eat burger", IsDone = true });
                Items.Add(new TodoItem() { Description = "Eat fries", IsDone = true });
                Items.Add(new TodoItem() { Description = "Drink water", IsDone = false });
                Items.Add(new TodoItem() { Description = "Drink coke", IsDone = true });
            }
            else
            {
                // Code runs "for real"
                // We are adding this so that our feature will pass, remove this after adding the "add" feature
                Items.Add(new TodoItem() { Description = "Eat burger", IsDone = true });
                Items.Add(new TodoItem() { Description = "Eat fries", IsDone = true });
                Items.Add(new TodoItem() { Description = "Drink water", IsDone = false });
                Items.Add(new TodoItem() { Description = "Drink coke", IsDone = true });
            }
        }
        
        
        

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RaisePropertyChanged(IsEmptyPlaceholderShownPropertyName);
        }

        /// <summary>
        /// The <see cref="Items" /> property's name.
        /// </summary>
        public const string ItemsPropertyName = "Items";

        private ObservableCollection<TodoItem> _items = new ObservableCollection<TodoItem>();

        /// <summary>
        /// Sets and gets the Items property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<TodoItem> Items
        {
            get{ return _items;}
            set{ Set(ItemsPropertyName, ref _items, value);}
        }

        /// <summary>
        /// The <see cref="IsEmptyPlaceholderShown" /> property's name.
        /// </summary>
        public const string IsEmptyPlaceholderShownPropertyName = "IsEmptyPlaceholderShown";

        /// <summary>
        /// Sets and gets the IsEmptyPlaceholderShown property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsEmptyPlaceholderShown
        {
            get
            {
                return Items.Count == 0;
            }
        }
    }
}