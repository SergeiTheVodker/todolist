﻿Feature: View

As a user I should be able to view the list of my todos

Scenario: View all todos
	Given the user have todos in the list
	When user opens the app
	Then user should see the list of todos
