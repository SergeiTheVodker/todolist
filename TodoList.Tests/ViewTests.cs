﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.ViewModel;

namespace TodoList.Tests
{
    [TestFixture]
    public class ViewTests
    {
        private MainViewModel mainVM;

        [Test]
        public void View_WhenThereIsNoItem_ShowNoItem()
        {
            // Arrange
            mainVM = new MainViewModel();

            // Act
            mainVM.Items.Clear();

            // Assert
            Assert.IsTrue(mainVM.IsEmptyPlaceholderShown);
        }
    }
}
