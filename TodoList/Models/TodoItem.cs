﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Models
{

    public class TodoItem:ViewModelBase
    {
        
         /// <summary>
        /// The <see cref="Status" /> property's name.
        /// </summary>
        public const string IsDonePropertyName = "IsDone";

        private bool _isDone;

        /// <summary>
        /// Sets and gets the Status property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsDone 
        {
            get
            {
                return _isDone;
            }
            set
            {
                Set(IsDonePropertyName, ref _isDone, value);
            }
        }
         
        public string Description { get; set; }
    }
}
